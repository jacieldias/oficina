<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>

<head>

<link type="text/css" rel="stylesheet" media="screen" href="<c:url value='/css/estilos_gerais.css'/>">
<link type="text/css" rel="stylesheet" media="screen" href="<c:url value='/css/cliente.css'/>">
<link type="text/css" rel="stylesheet" media="screen" href="<c:url value='/datatables/media/css/dataTables.bootstrap.css'/>">
<link type="text/css" rel="stylesheet" media="screen" href="<c:url value='/datatables/media/css/fixedColumns.bootstrap.css'/>">
<link type="text/css" rel="stylesheet" media="screen" href="<c:url value='/datatables/media/css/jquery.dataTables_themeroller.css'/>">


<script type="text/javascript" charset="utf8" src="<c:url value='/datadables/media/js/jquery.js'/>"></script>
<script type="text/javascript" charset="utf8" src="<c:url value='/datatables/media/js/jquery.dataTables.js'/>"></script>
<script type="text/javascript" charset="utf8" src="<c:url value='/datatables/media/js/dataTables.bootstrap.js'/>"></script>
<script type="text/javascript" charset="utf8" src="<c:url value='/js/dataTables.fixedColumns.js'/>"></script>
<script type="text/javascript" charset="utf8" src="<c:url value='/js/moment-with-locales.min.js'/>"></script>
<script type="text/javascript" charset="utf8" src="<c:url value='/js/cliente.js'/>"></script>
<script type="text/javascript" charset="utf8" src="<c:url value='/js/valida_cpf_cnpj.js'/>"></script>

<title>Cadastro dos Clientes</title>

</head>
<body>
<!-- Modal Mensagem de Sucesso -->
	<div id="myModal" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
						<h4 class="modal-title">Sucesso!</h4>
					</div>
					<div class="modal-body">
							<div id="msgSucesso">
		  		                 <p id="msgVazia">${mensagem}</p> 
		  	                </div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" data-dismiss="modal"><span class="glyphicon glyphicon-ok"></span> Ok</button>
					</div>
				</div>

			</div>
		</div>

	<div class="container">
		<h3>Cadastro dos Clientes</h3>
		<hr>			
		<form id="frmCliente" role="form" method="post" action="${linkTo[ClienteController].adiciona}">
							
				<div class="row">
					<div class="col-md-2">
						<div class="form-group">
							<label for="txtDataInclusao">Data Inclusão:</label> 
							<input type="text" class="form-control input-sm" id="txtDataInclusao" 
							 name="cliente.dataInclusao" value="${cliente.dataInclusao}">
						</div>
					</div>					
					<div class="col-md-2">
						<div class="form-group">
							<label id = "lblCpf" for="txtCpf">Cpf/Cnpj:</label> 
							<input type="text" class="form-control input-sm" id="txtCpf" name="cliente.cpf" value="${cliente.cpf}">
							<%-- <label id = "lblCnpj" for="txtCnpj">Cnpj:</label> 
							<input type="text" class="form-control input-sm" id="txtCnpj" name="cliente.cnpj" value="${cliente.cpf}"> --%>
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<label for="txtRg">RG/Insc. Est.:</label> 
							<input type="text" 	class="form-control input-sm" id="txtRg" name="cliente.rg" value="${cliente.rg}">
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label for="txtNome">Nome:</label> 
							<input type="text" 	class="form-control input-sm" id="txtNome" name="cliente.nome" value="${cliente.nome}">
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<label for="slcSituacao">Situação:</label> 
							<select class="form-control input-sm" id="slcSituacao" name="cliente.situacao">
									<option></option>
								<c:forEach items="${enumSituacao}" var="situacaoCliente">
									<option value="${situacaoCliente}">${situacaoCliente.label}</option>																		
								</c:forEach>
							</select>
						</div>						
					</div>
					
				</div>	
					
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">				
							<label for="txtLogradouro">Logradouro:</label> 
							<input type="text" 	class="form-control input-sm" id="txtLogradouro" name="cliente.logradouro" value="${cliente.logradouro}">
						</div>
					</div>
					<div class="col-md-1">
						<div class="form-group">
							<label for="txtNumero">Número:</label> 
							<input type="text" 	class="form-control input-sm" id="txtNumero" name="cliente.numero" value="${cliente.numero}">
						</div>
					</div>
					<div class="col-md-3">	
						<div class="form-group">
							<label for="txtBairro">Bairro:</label> 
							<input type="text" 	class="form-control input-sm" id="txtBairro" name="cliente.bairro" value="${cliente.bairro}">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">				
							<label for="txtCidade">Cidade:</label> 
							<input type="text" 	class="form-control input-sm" id="txtCidade" name="cliente.cidade" value="${cliente.cidade}">
						</div>
					</div>
					<div class="col-md-1">
						<div class="form-group">
							<label for="slcUf">Uf:</label> 
							<select class="form-control input-sm" id="slcUf" name="cliente.uf">
									<option></option>
								<c:forEach items="${enumEstados}" var="unidadefederal">
									<option value="${unidadefederal}">${unidadefederal.label}</option>									
								</c:forEach>
							</select>
						</div>						
					</div>
				</div>			
			
				<div class="row">
					<div class="col-md-2">
						<div class="form-group">				
							<label for="txtCep">Cep:</label> 
							<input type="text" 	class="form-control input-sm" id="txtCep" name="cliente.cep" value="${cliente.cep}">
						</div>
					</div>				
				
					<div class="col-md-2">
						<div class="form-group">				
							<label for="txtTelefone1">Telefone Fixo:</label> 
							<input type="text" 	class="form-control input-sm" id="txtTelefone1" name="cliente.telefone1" value="${cliente.telefone1}">
						</div>
					</div>
									
					<div class="col-md-2">
						<div class="form-group">				
							<label for="txtTelefone2">Celular 1:</label> 
							<input type="text" 	class="form-control input-sm" id="txtTelefone2" name="cliente.telefone2" value="${cliente.telefone2}">
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">				
							<label for="txtTelefone1">Celular 2:</label> 
							<input type="text" 	class="form-control input-sm" id="txtTelefone3" name="cliente.telefone3" value="${cliente.telefone3}">
						</div>
					</div>					
				</div>		
				
			<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-floppy-disk"></span> Salvar</button>
		</form>				
	</div>
	<br>
	<!-- Tabela Refente ao Cadastro dos Dados da Montadora Plugin usado para formatação da tabela: DataTable 1.10.7-->
	
	<div class="container">
		<table id="tbl_cliente"
			class="table table-striped table-bordered table-condensed table-hover display nowrap">
			<thead>
				<tr>
					<th width="10%">Id</th>					
					<th>Cpf/Cnpj</th>					
					<th>Nome</th>
					<th>Cidade</th>
					<th width="10%">Ações</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${clienteList}" var="cliente">
					<tr>
						<td class="tdCentralizado">${cliente.id}</td>						
						<td class="tdCentralizado">${cliente.cpf}</td>						
						<td>${cliente.nome}</td>						
						<td class="tdCentralizado">${cliente.cidade}</td>	
						
						<td class="tdCentralizado">
							<a href="${linkTo[ClienteController].alterar(cliente.id)}" class="btn btn-primary btn-xs"
								data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Alterar Cliente">
							<span class="glyphicon glyphicon-edit"></span></a> 
							
							<a href="${linkTo[ClienteController].consultar(cliente.id)}" class="btn btn-primary btn-xs btnconsultar"
								data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Consultar Cliente">
							<span class="glyphicon glyphicon-search"></span></a>
							
							<a href="${linkTo[ClienteController].excluir(cliente.id, cliente.nome)}" class="btn btn-danger btn-xs btnexcluir"
								data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Excluir Cliente">
							<span class="glyphicon glyphicon-trash"></span></a>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	<c:forEach items="${errors}" var="erro">
		${erro.category} - ${erro.message}<br/>
	</c:forEach>	
	
</body>
</html>

