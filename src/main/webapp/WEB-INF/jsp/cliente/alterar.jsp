<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>

<head>

<link type="text/css" rel="stylesheet" media="screen" href="<c:url value='/css/estilos_gerais.css'/>">
<link type="text/css" rel="stylesheet" media="screen" href="<c:url value='/css/alterar_cliente.css'/>">

<script type="text/javascript" charset="utf8" src="<c:url value='/js/alterar_cliente.js'/>"></script>
<script type="text/javascript" charset="utf8" src="<c:url value='/js/valida_cpf_cnpj.js'/>"></script>

<title>Alterar Clientes</title>

</head>
<body>
<!-- Modal Mensagem de Sucesso -->
	<div id="myModal" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
						<h4 class="modal-title">Sucesso!</h4>
					</div>
					<div class="modal-body">
							<div id="msgSucesso">
		  		                 <p id="mensagem"></p> 
		  	                </div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" data-dismiss="modal"><span class="glyphicon glyphicon-ok"></span> Ok</button>
					</div>
				</div>
			</div>
		</div>

	<div class="container">
		<h3>Alterar Clientes</h3>
		<hr>			
		<form id="frm_altera_cliente" role="form" method="post" action="${linkTo[ClienteController].atualiza}">
							
				<div class="row">
					<div class="col-md-2">
						<div class="form-group">
							<label for="txtDataInclusao">Data Inclusão:</label> 
							<input type="text" class="form-control input-sm tdCentralizado" name="cliente.dataInclusao" value="${cliente.dataInclusao}">
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<label for="txtCpf">Cpf/Cnpj:</label> 
							<input type="text" 	class="form-control input-sm tdCentralizado" name="cliente.cpf" value="${cliente.cpf}">
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<label for="txtRg">RG/Insc. Est.:</label> 
							<input type="text" 	class="form-control input-sm tdCentralizado" name="cliente.rg" value="${cliente.rg}">
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label for="txtNome">Nome:</label> 
							<input type="text" 	class="form-control input-sm tdCentralizado" name="cliente.nome" value="${cliente.nome}">
							<input type="hidden" class="form-control" id="txtId" name="cliente.id" value="${cliente.id}">
							<input type="hidden" class="form-control" id="txtVerificaConteudo">
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<label for="slcSituacao">Situação:</label> 
							<select class="form-control input-sm tdCentralizado" name="cliente.situacao">
								<option selected="selected"></option>
								<c:forEach items="${enumSituacao}" var="situacaoCliente">
									<option value="${situacaoCliente}"
									<c:if test="${cliente.situacao eq situacaoCliente}">selected="selected"</c:if>>${situacaoCliente.label}</option>																		
								</c:forEach>
							</select>
						</div>						
					</div>
				</div>	
					
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">				
							<label for="txtLogradouro">Logradouro:</label> 
							<input type="text" 	class="form-control input-sm tdCentralizado" name="cliente.logradouro" value="${cliente.logradouro}">
						</div>
					</div>
					<div class="col-md-1">
						<div class="form-group">
							<label for="txtNumero">Número:</label> 
							<input type="text" 	class="form-control input-sm tdCentralizado" name="cliente.numero" value="${cliente.numero}">
						</div>
					</div>
					<div class="col-md-3">	
						<div class="form-group">
							<label for="txtBairro">Bairro:</label> 
							<input type="text" 	class="form-control input-sm tdCentralizado" name="cliente.bairro" value="${cliente.bairro}">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">				
							<label for="txtCidade">Cidade:</label> 
							<input type="text" 	class="form-control input-sm tdCentralizado" name="cliente.cidade" value="${cliente.cidade}">
						</div>
					</div>
					<div class="col-md-1">
						<div class="form-group">
							<label for="slcUf">Uf:</label> 
							<select class="form-control input-sm tdCentralizado" name="cliente.uf">
								<option selected="selected"></option>
								<c:forEach items="${enumEstados}" var="unidadefederal">
									<option value="${unidadefederal}"
									<c:if test="${cliente.uf eq unidadefederal}">selected="selected"</c:if>>${unidadefederal.label}</option>									
								</c:forEach>
							</select>
						</div>						
					</div>
				</div>			
			
				<div class="row">
					<div class="col-md-2">
						<div class="form-group">				
							<label for="txtCep">Cep:</label> 
							<input type="text" 	class="form-control input-sm tdCentralizado" name="cliente.cep" value="${cliente.cep}">
						</div>
					</div>					
				
					<div class="col-md-2">
						<div class="form-group">				
							<label for="txtTelefone1">Telefone Fixo:</label> 
							<input type="text" 	class="form-control input-sm tdCentralizado" name="cliente.telefone1" value="${cliente.telefone1}">
						</div>
					</div>
									
					<div class="col-md-2">
						<div class="form-group">				
							<label for="txtTelefone2">Celular 1:</label> 
							<input type="text" 	class="form-control input-sm tdCentralizado" name="cliente.telefone2" value="${cliente.telefone2}">
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">				
							<label for="txtTelefone1">Celular 2:</label> 
							<input type="text" 	class="form-control input-sm tdCentralizado" name="cliente.telefone3" value="${cliente.telefone3}">
						</div>
					</div>
				</div>		
				
			<button id="btnSalvar" type="submit" class="btn btn-success"><span class="glyphicon glyphicon-floppy-disk"></span> Salvar</button>
			<a href="${linkTo[ClienteController].cliente}" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</a>
		</form>				
	</div>
	<br>	
</body>
</html>

