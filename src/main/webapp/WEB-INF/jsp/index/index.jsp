<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<head>

<link type="text/css" rel="stylesheet" media="screen" href="<c:url value='/css/estilos_gerais.css'/>">

<link type="text/css" rel="stylesheet" media="screen" href="<c:url value='/css/index.css'/>">
<link type="text/css" rel="stylesheet" media="screen" href="<c:url value='/datatables/media/css/dataTables.bootstrap.css'/>">
<link type="text/css" rel="stylesheet" media="screen" href="<c:url value='/datatables/media/css/fixedColumns.bootstrap.css'/>">
<link type="text/css" rel="stylesheet" media="screen" href="<c:url value='/datatables/media/css/jquery.dataTables_themeroller.css'/>">

<script type="text/javascript" charset="utf8" src="<c:url value='/datadables/media/js/jquery.js'/>"></script>
<script type="text/javascript" charset="utf8" src="<c:url value='/datatables/media/js/jquery.dataTables.js'/>"></script>
<script type="text/javascript" charset="utf8" src="<c:url value='/datatables/media/js/dataTables.bootstrap.js'/>"></script>
<script type="text/javascript" charset="utf8" src="<c:url value='/js/dataTables.fixedColumns.js'/>"></script>
<script type="text/javascript" charset="utf8" src="<c:url value='/js/index.js'/>"></script>


	<title>Status das O.S</title>
	
</head>
<body>
	<div class="container">
		<h3>Status das O.S</h3>
		<hr>
		<div class="row">
			<div class="col-md-3">
				<div class="panel-group">
					  <div style="height: 120px;" class="panel panel-success">
						    <div class="panel-heading">
						    	<h2>3</h2>					    						    	
						    	<h4>O.S Em Andamentos</h4>
						   </div>
					  </div>			  
				</div>
			</div>
			<div class="col-md-3">
				<div class="panel-group">
					  <div style="height: 120px;" class="panel panel-primary">					   
					    <div class="panel-heading">
					    	<h2>0</h2>
					    	<h4>O.S Fechadas</h4>
					    </div>
					  </div>			  
				</div>
			</div>
			<div class="col-md-3">
				<div class="panel-group">
					  <div style="height: 120px;" class="panel panel-danger">
					    <div class="panel-heading">
					    	<h2>0</h2>
					    	<h4>O.S Aguardando Aprovação</h4>
					    </div>
					  </div>			  
				</div>
			</div>
			<div class="col-md-3">
				<div class="panel-group">
					  <div style="height: 120px;" class="panel panel-info">
					    <div class="panel-heading">
					    	<h2>3</h2>
					    	<h4>O.S Previstas Para Hoje</h4>
					    </div>
					  </div>			  
				</div>
			</div>		
		</div>		
	</div>
	
	<br>
	<br>
	<br>
	<br>
	
	<div class="container">
		<table id="tbl_os"
			class="table table-striped table-bordered table-condensed table-hover display nowrap">
			<thead>
				<tr>
					<th width="14%">Data Entrada</th>					
					<th>Placa</th>					
					<th>Veiculo</th>
					<th>Cliente</th>
					<th>Previsão</th>
					<th width="14%">Visualizar O.S</th>
				</tr>
			</thead>
			<tbody>
				<%-- <c:forEach items="${clienteList}" var="cliente"> --%>
					<tr>
						<td class="tdCentralizado">26/10/2015</td>						
						<td class="tdCentralizado">HGO-4578</td>						
						<td class="tdCentralizado">CORSA SEDAN</td>						
						<td class="tdCentralizado">ESPEDITO ALVES LEAL LEAL DE SA GUIMARÃES</td>	
						<td class="tdCentralizado">26/10/2015</td>
						<td class="tdCentralizado">
							<a href="#" class="btn btn-primary btn-xs"
								data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Alterar Cliente">
							<span class="glyphicon glyphicon-search"></span></a> 
						</td>
					</tr>
					<tr>
						<td class="tdCentralizado">26/10/2015</td>						
						<td class="tdCentralizado">XBP-2271</td>						
						<td class="tdCentralizado">GOL</td>						
						<td class="tdCentralizado">JOÃO ALVES DA SILVA</td>	
						<td class="tdCentralizado">26/10/2015</td>
						<td class="tdCentralizado">
							<a href="#" class="btn btn-primary btn-xs"
								data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Alterar Cliente">
							<span class="glyphicon glyphicon-search"></span></a> 
						</td>
					</tr>
					<tr>
						<td class="tdCentralizado">26/10/2015</td>						
						<td class="tdCentralizado">MET-7849</td>						
						<td class="tdCentralizado">PRISMA</td>						
						<td class="tdCentralizado">HORÁCIO MATIAS SOBRINHO NETO</td>	
						<td class="tdCentralizado">26/10/2015</td>
						<td class="tdCentralizado">
							<a href="#" class="btn btn-primary btn-xs"
								data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Alterar Cliente">
							<span class="glyphicon glyphicon-search"></span></a> 
						</td>
					</tr>
					<tr>
						<td class="tdCentralizado">26/10/2015</td>						
						<td class="tdCentralizado">KQL-0321</td>						
						<td class="tdCentralizado">PALIO WEEKEND</td>						
						<td class="tdCentralizado">JACIEL DIAS DE SOUZA</td>	
						<td class="tdCentralizado">26/10/2015</td>
						<td class="tdCentralizado">
							<a href="#" class="btn btn-primary btn-xs"
								data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Alterar Cliente">
							<span class="glyphicon glyphicon-search"></span></a> 
						</td>
					</tr>
					<tr>
						<td class="tdCentralizado">26/10/2015</td>						
						<td class="tdCentralizado">HUL-7452</td>						
						<td class="tdCentralizado">SAVEIRO</td>						
						<td class="tdCentralizado">FRANCISCO BARBOSA DA SILVA</td>	
						<td class="tdCentralizado">26/10/2015</td>
						<td class="tdCentralizado">
							<a href="#" class="btn btn-primary btn-xs"
								data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Alterar Cliente">
							<span class="glyphicon glyphicon-search"></span></a> 
						</td>
					</tr>
					<tr>
						<td class="tdCentralizado">26/10/2015</td>						
						<td class="tdCentralizado">RSA-8125</td>						
						<td class="tdCentralizado">COROLLA</td>						
						<td class="tdCentralizado">FRANCISCO BARBOSA DA SILVA</td>	
						<td class="tdCentralizado">26/10/2015</td>
						<td class="tdCentralizado">
							<a href="#" class="btn btn-primary btn-xs"
								data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Alterar Cliente">
							<span class="glyphicon glyphicon-search"></span></a> 
						</td>
					</tr>
					<tr>
						<td class="tdCentralizado">26/10/2015</td>						
						<td class="tdCentralizado">FQA-9977</td>						
						<td class="tdCentralizado">GRAN SIENA</td>						
						<td class="tdCentralizado">EDSON DIAS DA SILVA</td>	
						<td class="tdCentralizado">26/10/2015</td>
						<td class="tdCentralizado">
							<a href="#" class="btn btn-primary btn-xs"
								data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Alterar Cliente">
							<span class="glyphicon glyphicon-search"></span></a> 
						</td>
					</tr>
									
				<%-- </c:forEach> --%>
			</tbody>
		</table>
	</div>
</body>
</html>