<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<head>
	
<link type="text/css" rel="stylesheet" media="screen" href="<c:url value='/css/estilos_gerais.css'/>">
<link type="text/css" rel="stylesheet" media="screen" href="<c:url value='/css/alterar_veiculo.css'/>">

<script type="text/javascript" charset="utf8" src="<c:url value='/js/alterar_veiculo.js'/>"></script>

<title>Alterar Modelos dos Veículos</title>

</head>
<body>
<!-- Modal Mensagem de Sucesso -->
	<div id="myModal" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
						<h4 class="modal-title">Sucesso!</h4>
					</div>
					<div class="modal-body">
							<div id="msgSucesso">
		  		                 <p id="mensagem"></p> 
		  	                </div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" data-dismiss="modal"><span class="glyphicon glyphicon-ok"></span> Ok</button>
					</div>
				</div>
			</div>
		</div>	
	<div class="container">
		<h3>Alterar Modelos dos Veículos</h3>
		<hr>
		<form id="frm_altera_veiculo" role="form" method="post" action="${linkTo[VeiculoController].atualiza}">
							
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">					
							<label for="txtDescricao">Descrição:</label> 
							<input type="text" 	class="form-control input-sm" id="txtDescricao" name="veiculo.descricao" value="${veiculo.descricao}">
							<input type="hidden" class="form-control" id="txtId" name="veiculo.id" value="${veiculo.id}">
							<input type="hidden" class="form-control" id="txtVerificaConteudo">
						</div>
					</div>	
					<div class="col-md-4">
						<div class="form-group">						
							<label for="slcMontadora">Montadora:</label> 
							<select class="form-control input-sm" id="slcMontadora" name="veiculo.montadora.id">
								<option value="${veiculo.montadora.id}">${veiculo.montadora.descricao}</option>
								<c:forEach items="${montadoraList}" var="montadora">
									<option value="${montadora.id}">${montadora.descricao}</option>
								</c:forEach>
							</select>
						</div>
					</div>	
					<div class="col-md-4">
						<div class="form-group">  
							<label for="slcCategoria">Categoria:</label> 
							<select class="form-control input-sm" id="slcCategoria" name="veiculo.categoriaVeiculo.id">
								<option value="${veiculo.categoriaVeiculo.id}">${veiculo.categoriaVeiculo.descricao}</option>
								<c:forEach items="${categoriaList}" var="categoria">
									<option value="${categoria.id}">${categoria.descricao}</option>
								</c:forEach>
							</select>
					    </div>
				   </div>	
			</div>	
			<button id="btnSalvar" type="submit" class="btn btn-success"><span class="glyphicon glyphicon-floppy-disk"></span> Salvar</button>
			<a href="${linkTo[VeiculoController].veiculo}" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</a>
		</form>	
	</div>
	<br>
</body>
</html>

