<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<head>
		
	<title>Auto Max Control</title>
	
<style type="text/css" media="all">
th {
	text-align: center;
}

#txtDescricao {
	width: 40%;
}

h3 {
	margin-top: 70px;
}

</style>

<script type="text/javascript">
		
		$(document).ready(function() {
			$('#teste').click(function(e){
				
				$('#myModal').modal('show');
			});
        });

</script>

</head>
<body>
	
	<div class="container">
	<h3>Tela Teste</h3>
	<hr>
	<p>TESTANDO O PROJETO O LAYOUT DA PÁGINA TESTE</p>
	</div>
	<div class="container">

		<!-- Trigger the modal with a button -->
		<button id="teste" type="button" class="btn btn-info btn-lg" data-toggle="modal">Testar Modal</button>

		<!-- Modal -->
		<div id="myModal" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Alterar Montadora</h4>
					</div>
					<div class="modal-body">
						<div class="container">
							<form role="form" method="post"
								action="${linkTo[MontadoraController].atualiza}">
								<div class="form-group">
									<label for="txtDescricao">Descrição:</label> <input type="text"
										class="form-control" id="txtDescricao"
										name="montadora.descricao" value="${montadora.descricao}">
									<input type="hidden" class="form-control" id="txtId"
										name="montadora.id" value="${montadora.id}">
								</div>
								<button type="submit" class="btn btn-primary">Salvar</button>
							</form>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>

			</div>
		</div>

	</div>
</body>
</html>