/* Códigos Jquery referentes ao comportamento da view de alteração dos dados das categorias dos veiculos.
---------------------------------------------------------------------------------------------------------------------*/

$(document).ready(function() {
				
			$('#txtDescricao').change(function(){
				var valorRecebido = $('#txtDescricao').val();
				$('#txtVerificaConteudo').val(valorRecebido);
				$('#mensagem').html('Categoria ' + valorRecebido + ' alterada com sucesso!')			
			});
			
			$('#btnSalvar').click(function(){
				if($('#txtVerificaConteudo').val() == ''){
		
					$('#msgSucesso').hide();
		
					}else{
						
						$('#myModal').modal('show');					
					}	
			});
	
			$('#frm_alterar_categoria_veiculo').validate({
			    rules: {
			    	"categoriaVeiculo.descricao": {
			            required: true
			            		            
			        },
		        
		    },
		    highlight: function(element) {
		        $(element).closest('.form-group').addClass('has-error');
		    },
		    unhighlight: function(element) {
		        $(element).closest('.form-group').removeClass('has-error');
		    },
		    errorElement: 'span',
		    errorClass: 'help-block',
		    errorPlacement: function(error, element) {
		        if(element.parent('.input-group').length) {
		            error.insertAfter(element.parent());
		        } else {
		            error.insertAfter(element);
		        }
		    },

		    messages: {
	        	"categoriaVeiculo.descricao": {
	                required: "Este campo não pode ser vazio. Digite o nome de uma categoria!", 
	                
	            },
	            
	        } 
		    
		});				
	});