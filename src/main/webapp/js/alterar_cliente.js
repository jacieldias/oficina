/* Códigos Jquery referentes ao comportamento da view de alteração dos dados dos clientes.
---------------------------------------------------------------------------------------------------------------------*/

$(document).ready(function() {
				
		$('#txtCpf').change(function(){
			var valorRecebido = $('#txtCpf').val();
			$('#txtVerificaConteudo').val(valorRecebido);
			$('#mensagem').html('Cpf/Cnpj do cliente: ' + valorRecebido + ' alterado com sucesso!')			
		});
		
		$('#txtRg').change(function(){
			var valorRecebido = $('#txtRg').val();
			$('#txtVerificaConteudo').val(valorRecebido);
			$('#mensagem').html('Rg do cliente: ' + valorRecebido + ' alterado com sucesso!')			
		});
		
		$('#txtNome').change(function(){
			var valorRecebido = $('#txtNome').val();
			$('#txtVerificaConteudo').val(valorRecebido);
			$('#mensagem').html('Nome do cliente: ' + valorRecebido + ' alterado com sucesso!')			
		});
		
				
		$('#slcSituacao').change(function(){
			var valorRecebido = $('#slcSituacao option:selected').text();
			$('#txtVerificaConteudo').val(valorRecebido);
			$('#mensagem').html('Situação do cliente: ' + valorRecebido + ' alterada com sucesso!')			
		});
		
		$('#txtLogradouro').change(function(){
			var valorRecebido = $('#txtLogradouro').val();
			$('#txtVerificaConteudo').val(valorRecebido);
			$('#mensagem').html('Logradouro do cliente: ' + valorRecebido + ' alterado com sucesso!')			
		});
		
		$('#txtNumero').change(function(){
			var valorRecebido = $('#txtNumero').val();
			$('#txtVerificaConteudo').val(valorRecebido);
			$('#mensagem').html('Número do cliente: ' + valorRecebido + ' alterado com sucesso!')			
		});
		
		$('#txtBairro').change(function(){
			var valorRecebido = $('#txtBairro').val();
			$('#txtVerificaConteudo').val(valorRecebido);
			$('#mensagem').html('Bairro do cliente: ' + valorRecebido + ' alterado com sucesso!')			
		});
		
		$('#txtCidade').change(function(){
			var valorRecebido = $('#txtCidade').val();
			$('#txtVerificaConteudo').val(valorRecebido);
			$('#mensagem').html('Cidade do cliente: ' + valorRecebido + ' alterado com sucesso!')			
		});
		
		$('#slcUf').change(function(){
			var valorRecebido = $('#slcUf option:selected').text();
			$('#txtVerificaConteudo').val(valorRecebido);
			$('#mensagem').html('Uf do cliente: ' + valorRecebido + ' alterada com sucesso!')			
		});
		
		$('#txtCep').change(function(){
			var valorRecebido = $('#txtCep').val();
			$('#txtVerificaConteudo').val(valorRecebido);
			$('#mensagem').html('CEP do cliente: ' + valorRecebido + ' alterado com sucesso!')			
		});
		
		$('#txtTelefone1').change(function(){
			var valorRecebido = $('#txtTelefone1').val();
			$('#txtVerificaConteudo').val(valorRecebido);
			$('#mensagem').html('Telefone fixo do cliente: ' + valorRecebido + ' alterado com sucesso!')			
		});
		
		$('#txtTelefone2').change(function(){
			var valorRecebido = $('#txtTelefone2').val();
			$('#txtVerificaConteudo').val(valorRecebido);
			$('#mensagem').html('Celular 1 do cliente: ' + valorRecebido + ' alterado com sucesso!')			
		});
		
		$('#txtTelefone3').change(function(){
			var valorRecebido = $('#txtTelefone3').val();
			$('#txtVerificaConteudo').val(valorRecebido);
			$('#mensagem').html('Celular 2 do cliente: ' + valorRecebido + ' alterado com sucesso!')			
		});
		
		
		
		
		$('#btnSalvar').click(function(){
			if($('#txtVerificaConteudo').val() == ''){

				$('#msgSucesso').hide();

				}else{
					
					$('#myModal').modal('show');					
				}	
		});
		
		

		$('#frm_altera_cliente').validate({
		    rules: {
		    	
		    	"cliente.nome": {
		            required: true		           	            
		        },
		        "cliente.logradouro": {
		            required: true		           	            
		        },
		        "cliente.bairro": {
		            required: true		           	            
		        },
		        "cliente.cidade": {
		            required: true		           	            
		        },
		        "cliente.telefone1":{
		        	required: true
		        },  		    		    
		        
		    },
		    highlight: function(element) {
		        $(element).closest('.form-group').addClass('has-error');
		    },
		    unhighlight: function(element) {
		        $(element).closest('.form-group').removeClass('has-error');
		    },
		    errorElement: 'span',
		    errorClass: 'help-block',
		    errorPlacement: function(error, element) {
		        if(element.parent('.input-group').length) {
		            error.insertAfter(element.parent());
		        } else {
		            error.insertAfter(element);
		        }
		    },

		    messages: {
		    	
		    	"cliente.nome": {
	                required: "Este campo não pode ser vazio. Digite o nome de um cliente!"	                
	            },
	            "cliente.logradouro": {
	                required: "Este campo não pode ser vazio. Digite o logradouro do cliente!"	                
	            },
	            "cliente.bairro":{
	            	required: "Este campo não pode ser vazio. Digite o bairro do endereço do cliente!"
	            },
	            "cliente.cidade":{
	            	required: "Este campo não pode ser vazio. Digite a cidade do cliente!"
	            },
	            "cliente.telefone1":{
	            	required: "Informe no mínimo um telefone!"
	            },           
	            
	        }, 
		    
		});

/* Código para as mascáras dos inputs. 
		---------------------------------------------------------------------------------------------------------------------*/
		$(function(){		
			
			$('#txtCpf').focusout(function(){
				
				var cpfcnpj, element;
				element = $(this);
				element.unmask();
				
				cpfcnpj = element.val().replace(/\D/g, '');
							
				if(cpfcnpj.length > 11){
					
					element.mask("99.999.999/9999-99");
					$('#txtCpf').rules("add",{
						cnpj:true,
						messages:{
							cnpj: "Cnpj inválido!"
						}
					})
					
					$('#txtCpf').rules("remove","cpf");
										
			    }else {
				
			    	element.mask("999.999.999-999-99");	
			    	$('#txtCpf').rules("add",{
						cpf:true,
						messages:{
							cpf: "Cpf inválido!"
						}
					})
					
					$('#txtCpf').rules("remove","cnpj");
			    }	
						
			}).trigger('focusout');		
			
			});	
		
			$('#txtDataInclusao').mask('99/99/9999');			
			$('#txtCep').mask('99.999-999');
			$('#txtTelefone1').mask('(99) 9999-9999');
			$('#txtTelefone2').mask('(99) 99999-9999');
			$('#txtTelefone3').mask('(99) 99999-9999');
				

/* Código para formatação das datas usando o plugin Moment.js 
---------------------------------------------------------------------------------------------------------------------*/		
				
	   /* moment.lang('pt-br');
		$('#txtDataInclusao').val(moment().format('L'));*/
				
		/*var d = new Date();
		dataHora= (d.toLocaleString());
				
		$('#txtDataInclusao').val();
		$('#txtDataInclusao').val(dataHora);*/
				
						
		/*$(function(){
					
		var data = new Date();
					
		dia = data.getDate(),
		mes = data.getMonth()+1;
		ano = data.getFullYear();
					
		var dataFormatada = dia + "/" + mes + "/" + ano; 
		$('#txtDataInclusao').val(dataFormatada);
	    })*/
		
	});	
