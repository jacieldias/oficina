/* Códigos Jquery referentes ao comportamento da view index.
---------------------------------------------------------------------------------------------------------------------*/

/*Formata o DataTable com os dados das O.S.
---------------------------------------------------------------------------------------------------------------------*/

$(document).ready(function() {
	
	table = $('#tbl_os').DataTable(
			{
			 "lengthMenu" : [5, 15, 25],
			 "paging" : true,
			 "ordering" : true,
			 "info" : true,
			 "scrollX" : true,
			 
			    
			 "columnDefs" : [
			    {"orderable" : false, "targets" : 5}             
			  ],
			  
			 "language" :
			 {
				"url" : "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Portuguese-Brasil.json"
			 }
		});
		
/* Código para definir o comportamento da mensagem quando a view for chamada. 
---------------------------------------------------------------------------------------------------------------------*/		
		
			/*if($('#msgVazia').is(':empty')){ 
				
			    $('#msgSucesso').hide();
		    }else{
			    $('#myModal').modal('show');
		    } */
			
/* Código para fechar a mensagem quando o usuário clicar no input. 
---------------------------------------------------------------------------------------------------------------------*/
		
		/*$('#txtDescricao').focus(function(){
			$('#msgSucesso').hide();
		});*/
		
		
/* Código para a confirmação da ação caso o usuário clique no botão de excluir. 
---------------------------------------------------------------------------------------------------------------------*/		

		/*$('.btnexcluir').click(function (e){
		    if (confirm("Tem certeza que deseja excluir este cliente?")) { 		    	
		  		window.location.href = "remove?id="; 
		  		
		    } else {
		        alert('Operação cancelada!');
		        e.preventDefault();
		    }		   
			
		});*/
		
/* Time da mensagem caso o usuário não clique no input ou no botão de fechar. 
---------------------------------------------------------------------------------------------------------------------*/		

		/*setTimeout("$('#msgSucesso').hide();",3000);*/


/* Código para a validação do input. 
---------------------------------------------------------------------------------------------------------------------*/
		/*$('#frmCliente').validate({
		    rules: {
		    	"cliente.nome": {
		            required: true		           	            
		        },
		        "cliente.logradouro": {
		            required: true		           	            
		        },
		        "cliente.bairro": {
		            required: true		           	            
		        },
		        "cliente.cidade": {
		            required: true		           	            
		        },
		        "cliente.telefone1":{
		        	required: true
		        },     
		        
		    },

		    highlight: function(element) {
		        $(element).closest('.form-group').addClass('has-error');
		    },
		    unhighlight: function(element) {
		        $(element).closest('.form-group').removeClass('has-error');
		    },
		    errorElement: 'span',
		    errorClass: 'help-block',
		    errorPlacement: function(error, element) {
		        if(element.parent('.input-group').length) {
		            error.insertAfter(element.parent());
		        } else {
		            error.insertAfter(element);
		        }
		    },

		    messages: {
	        	"cliente.nome": {
	                required: "Este campo não pode ser vazio. Digite o nome de um cliente!"	                
	            },
	            "cliente.logradouro": {
	                required: "Este campo não pode ser vazio. Digite o logradouro do cliente!"	                
	            },
	            "cliente.bairro":{
	            	required: "Este campo não pode ser vazio. Digite o bairro do endereço do cliente!"
	            },
	            "cliente.cidade":{
	            	required: "Este campo não pode ser vazio. Digite a cidade do cliente!"
	            },
	            "cliente.telefone1":{
	            	required: "Informe no mínimo um telefone!"
	            },	            
	            
	        }
		    
		});	*/
		
				
/* Código para as mascáras dos inputs. 
---------------------------------------------------------------------------------------------------------------------*/
		/*$(function(){		
			
		$('#txtCpf').focusout(function(){
			
			var cpfcnpj, element;
			element = $(this);
			element.unmask();
			
			cpfcnpj = element.val().replace(/\D/g, '');
						
			if(cpfcnpj.length > 11){
				
				element.mask("99.999.999/9999-99");
				$('#txtCpf').rules("add",{
					cnpj:true,
					messages:{
						cnpj: "Cnpj inválido!"
					}
				})
				
				$('#txtCpf').rules("remove","cpf");
									
		    }else {
			
		    	element.mask("999.999.999-999-99");	
		    	$('#txtCpf').rules("add",{
					cpf:true,
					messages:{
						cpf: "Cpf inválido!"
					}
				})
				
				$('#txtCpf').rules("remove","cnpj");
		    }	
					
		}).trigger('focusout');		
		
		});	
		
		$('#txtDataInclusao').mask('99/99/9999');
		$('#txtCep').mask('99.999-999');
		$('#txtTelefone1').mask('(99) 9999-9999');
		$('#txtTelefone2').mask('(99) 99999-9999');
		$('#txtTelefone3').mask('(99) 99999-9999');
				*/

/* Código para formatação das datas usando o plugin Moment.js 
---------------------------------------------------------------------------------------------------------------------*/		
		
		/*moment.lang('pt-br');
		$('#txtDataInclusao').val(moment().format('L'));	*/
		

/* Código usado para emitir as mensagens nos botões de alterar, excluir e consultar.js 
		---------------------------------------------------------------------------------------------------------------------*/
		
		
		/*$('[data-toggle="popover"]').popover();*/
		
});

