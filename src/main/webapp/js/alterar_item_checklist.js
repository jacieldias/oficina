/* Códigos Jquery referentes ao comportamento da view de alteração dos dados das montadoras.
---------------------------------------------------------------------------------------------------------------------*/

$(document).ready(function() {
				
		$('#txtDescricao').change(function(){
			var valorRecebido = $('#txtDescricao').val();
			$('#txtVerificaConteudo').val(valorRecebido);
			$('#mensagem').html('Item do checklist ' + valorRecebido + ' alterado com sucesso!')			
		});

		$('#btnSalvar').click(function(){
			if($('#txtVerificaConteudo').val() == ''){

				$('#msgSucesso').hide();

				}else{
					
					$('#myModal').modal('show');
				}			
		});

		$('#frm_alterar_itens_checklist').validate({
		    rules: {
		    	"itemChecklist.descricao": {
		            required: true
		            		            
		        },
		        
		    },
		    highlight: function(element) {
		        $(element).closest('.form-group').addClass('has-error');
		    },
		    unhighlight: function(element) {
		        $(element).closest('.form-group').removeClass('has-error');
		    },
		    errorElement: 'span',
		    errorClass: 'help-block',
		    errorPlacement: function(error, element) {
		        if(element.parent('.input-group').length) {
		            error.insertAfter(element.parent());
		        } else {
		            error.insertAfter(element);
		        }
		    },

		    messages: {
	        	"itemChecklist.descricao": {
	                required: "Este campo não pode ser vazio. Digite o nome de um item do checklist!", 
	                
	            },
	            
	        } 
		    
		});				
	});