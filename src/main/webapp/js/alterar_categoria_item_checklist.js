/* Códigos Jquery referentes ao comportamento da view de alteração das categorias dos dados das montadoras.
---------------------------------------------------------------------------------------------------------------------*/

$(document).ready(function() {				
		
		$('#txtDescricao').change(function(){
			var valorRecebido = $('#txtDescricao').val();
			$('#txtVerificaConteudo').val(valorRecebido);
			$('#mensagem').html('Categoria do item do checklist ' + valorRecebido + ' alterada com sucesso!')			
		});

		$('#btnSalvar').click(function(){
			if($('#txtVerificaConteudo').val() == ''){

				$('#msgSucesso').hide();

				}else{
					
					$('#myModal').modal('show');
				}			
		});

		$('#frm_alterar_categoria_itens_checklist').validate({
		    rules: {
		    	"categoriaItemChecklist.descricao": {
		            required: true
		            		            
		        },
		        
		    },
		    highlight: function(element) {
		        $(element).closest('.form-group').addClass('has-error');
		    },
		    unhighlight: function(element) {
		        $(element).closest('.form-group').removeClass('has-error');
		    },
		    errorElement: 'span',
		    errorClass: 'help-block',
		    errorPlacement: function(error, element) {
		        if(element.parent('.input-group').length) {
		            error.insertAfter(element.parent());
		        } else {
		            error.insertAfter(element);
		        }
		    },

		    messages: {
	        	"categoriaItemChecklist.descricao": {
	                required: "Este campo não pode ser vazio. Digite o nome de uma categoria do item do checklist!", 
	                
	            },
	            
	        } 
		    
		});				
	});