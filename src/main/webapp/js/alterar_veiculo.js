/* Códigos Jquery referentes ao comportamento da view de alteração dos dados dos modelos dos veiculos.
---------------------------------------------------------------------------------------------------------------------*/

$(document).ready(function() {
				
		$('#txtDescricao').change(function(){
			var valorRecebido = $('#txtDescricao').val();
			$('#txtVerificaConteudo').val(valorRecebido);
			$('#mensagem').html('Descrição do veículo: ' + valorRecebido + ' alterada com sucesso!')			
		});
		
		
		$('#slcMontadora').change(function(){
			var valorRecebido = $('#slcMontadora option:selected').text();
			$('#txtVerificaConteudo').val(valorRecebido);
			$('#mensagem').html('Montadora do veículo: ' + valorRecebido + ' alterada com sucesso!')			
		});
		
		$('#slcCategoria').change(function(){
			var valorRecebido = $('#slcCategoria option:selected').text();
			$('#txtVerificaConteudo').val(valorRecebido);
			$('#mensagem').html('Categoria do veículo: ' + valorRecebido + ' alterada com sucesso!')			
		});

		$('#btnSalvar').click(function(){
			if($('#txtVerificaConteudo').val() == ''){

				$('#msgSucesso').hide();

				}else{
					
					$('#myModal').modal('show');
				}			
		});
		
		

		$('#frm_altera_veiculo').validate({
		    rules: {
		    	"veiculo.descricao": {
		            required: true		            		            
		    	},		    		    
		        
		    },
		    highlight: function(element) {
		        $(element).closest('.form-group').addClass('has-error');
		    },
		    unhighlight: function(element) {
		        $(element).closest('.form-group').removeClass('has-error');
		    },
		    errorElement: 'span',
		    errorClass: 'help-block',
		    errorPlacement: function(error, element) {
		        if(element.parent('.input-group').length) {
		            error.insertAfter(element.parent());
		        } else {
		            error.insertAfter(element);
		        }
		    },

		    messages: {
	        	"veiculo.descricao": {
	                required: "Este campo não pode ser vazio. Digite o nome de um veículo!",	                
	            },           
	            
	        }, 
		    
		});				
	});	
