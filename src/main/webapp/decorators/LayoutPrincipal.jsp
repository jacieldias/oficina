<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>

<!DOCTYPE html>

<html lang="pt_BR">
<head>
	<meta charset="UTF-8">
	
	<title><decorator:title default="Auto Max Control"/></title>
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<link type="text/css" rel="stylesheet" media="screen" href="<c:url value='/bootstrap/css/bootstrap.min.css'/>"/>
	<link type="text/css" rel="stylesheet" media="screen" href="<c:url value='/bootstrap/css/bootstrap-theme.min.css'/>"/>
	<link type="text/css" rel="stylesheet" media="screen" href="<c:url value='/bootstrap/css/bootstrap-submenu.min.css'/>"/>
	
	<script type="text/javascript" charset="utf8" src="<c:url value='/js/jquery-2.1.4.js'/>"></script>	
	<script type="text/javascript" charset="utf8" src="<c:url value='/bootstrap/js/bootstrap.min.js'/>"></script>
	<script type="text/javascript" charset="utf8" src="<c:url value='/bootstrap/js/bootstrap-submenu.min.js'/>"></script>
	<script type="text/javascript" charset="utf8" src="<c:url value='/js/jquery.validate.js'/>"></script>
	<script type="text/javascript" charset="utf8" src="<c:url value='/js/jquery.mask.min.js'/>"></script>
	
	
<style type="text/css" media="all">

#footer{
		margin-top: 27px;		 
 }
 
 #txtFooter{
    text-align: center;
  }
</style>	

<script type="text/javascript">
	
		$(function() {
			$('.dropdown-toggle').dropdown();
			$('.dropdown-submenu > a').submenupicker();	
		});
    
</script>
	<decorator:head></decorator:head>
</head>

<body>
	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">Auto Max Control</a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li class="active"><a href="${linkTo[IndexController].index}">Início<span class="sr-only">(current)</span></a></li>
					<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Cadastro<span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="${linkTo[ClienteController].cliente}">Clientes</a></li>								
							<li class="dropdown-submenu"><a tabindex="0" data-toggle="dropdown" aria-expanded="false">Veículos</a>
								<ul class="dropdown-menu">
									<li><a href="${linkTo[CaracteristicaVeiculoController].caracteristicaVeiculo}">Características</a></li>
									<li><a href="${linkTo[CategoriaVeiculoController].categoriaVeiculo}">Categorias</a></li>
									<li><a href="${linkTo[VeiculoController].veiculo}">Modelos</a></li>							
								</ul>	
							</li>
							<li><a href="${linkTo[IndexController].login}">Usuários</a></li>
							<li class="divider"></li>
							<li><a href="#">Funcionários</a></li>
							<li class="divider"></li>
							<li><a href="${linkTo[TesteController].teste}">Serviços</a></li>
							<li><a href="${linkTo[MontadoraController].montadora}">Montadoras</a></li>					
						</ul>
					</li>		
					<li><a href="#"><span>Relatório</span></a></li>
					<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Checklist<span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#">Checklist do Veículo (Entrada)</a></li>
							<li><a href="#">Ckecklist do Veículo (Saída)</a></li>
							<li class="divider"></li>
							<li><a href="${linkTo[ItemChecklistController].itemChecklist}">Item do Checklist</a></li>							
							<li><a href="${linkTo[CategoriaItemChecklistController].categoriaItemChecklist}">Categoria do Item Checklist</a></li>							
						</ul>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="#"><span class="glyphicon glyphicon-user"></span> Usuário</a></li>
					<li><a href="${linkTo[IndexController].login}"><span class="glyphicon glyphicon-log-out"></span> Log-out</a></li>
			    </ul>
			</div>
	  </div>
	</div>
	<div class="container-fluid">
	    <decorator:body/>
    </div>
	<div class="navbar navbar-static-bottom">
		<div class="container">
			<div class="row-fluid">
				<div class="span12">
					<footer id="footer" >
					<hr>
						<p id="txtFooter">&copy; 2015 <a target="_blank" href="http://www.viattec.com.br" title="Viattec Sistemas Ltda">Viattec - Soluções Tecnológicas em Software Ltda</a>. Todos os Direitos Reservados.</p>
					</footer>
				</div>
			</div>
		</div>
	</div>
</body>

</html>