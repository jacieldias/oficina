package br.com.viattec.automaxcontrol.test;

import java.util.ArrayList;
import java.util.List;

import br.com.viattec.automaxcontrol.dao.CaracteristicaVeiculoDao;
import br.com.viattec.automaxcontrol.dao.MontadoraDao;
import br.com.viattec.automaxcontrol.model.CaracteristicaVeiculo;
import br.com.viattec.automaxcontrol.model.CategoriaVeiculo;
import br.com.viattec.automaxcontrol.model.Montadora;
import br.com.viattec.automaxcontrol.util.JPAUtil;

public class TesteMontadora {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Montadora montadora = new Montadora("HYUNDAI");
		MontadoraDao dao = new MontadoraDao(JPAUtil.getEntityManager());
		dao.add(montadora);
		
		CaracteristicaVeiculo cv = new CaracteristicaVeiculo("AR-CONDICIONADO");
		CaracteristicaVeiculoDao dao2 = new CaracteristicaVeiculoDao(JPAUtil.getEntityManager());
		dao2.add(cv);
	}

}
