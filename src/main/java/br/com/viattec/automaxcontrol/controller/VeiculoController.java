package br.com.viattec.automaxcontrol.controller;

import java.util.List;

import javax.inject.Inject;
import javax.validation.Valid;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.validator.Validator;
import br.com.viattec.automaxcontrol.dao.CategoriaVeiculoDao;
import br.com.viattec.automaxcontrol.dao.MontadoraDao;
import br.com.viattec.automaxcontrol.dao.VeiculoDao;
import br.com.viattec.automaxcontrol.model.CategoriaVeiculo;
import br.com.viattec.automaxcontrol.model.Montadora;
import br.com.viattec.automaxcontrol.model.Veiculo;

@Controller
@Path("/veiculo")
public class VeiculoController {

	private final Result result;
	private final VeiculoDao dao;
	private final Validator validator;
	private final MontadoraDao montadoraDao;
	private final CategoriaVeiculoDao categoriaVeiculoDao;

	@Inject
	public VeiculoController(Result result,VeiculoDao dao, Validator validator, MontadoraDao montadoraDao, CategoriaVeiculoDao categoriaVeiculoDao) {
		this.result = result;
		this.dao = dao;
		this.validator = validator;
		this.montadoraDao = montadoraDao;
		this.categoriaVeiculoDao = categoriaVeiculoDao;
	}	
	public VeiculoController() {
		this(null, null, null, null, null);
	}
	@Get("/veiculo")
	public void veiculo() {
		List<Veiculo> lista = dao.lista();
		List<Montadora> listaMontadora = montadoraDao.lista();
		List<CategoriaVeiculo> listaCategoria = categoriaVeiculoDao.lista();
		result.include("veiculoList",lista);
		result.include("montadoraList",listaMontadora);
		result.include("categoriaList",listaCategoria);
	}
	@Post
	public void adiciona(@Valid Veiculo veiculo) {
		validator.onErrorForwardTo(this).veiculo();
		dao.add(veiculo);
		result.include("mensagem","Veiculo modelo: " + veiculo.getDescricao() + " cadastrado com sucesso!");
		result.redirectTo(this).veiculo();
	}
	@Get("/alterar/{id}")
	public void alterar(Long id) {
		Veiculo veiculoEncrontrado = dao.buscaPorId(id);
		List<Montadora> listaMontadora = montadoraDao.lista();
		List<CategoriaVeiculo> listaCategoria = categoriaVeiculoDao.lista();
		result.include("veiculo",veiculoEncrontrado);
		result.include("montadoraList",listaMontadora);
		result.include("categoriaList",listaCategoria);
	}
	@Post
	public void atualiza(Veiculo veiculo) {
		dao.update(veiculo);
		result.redirectTo(this).veiculo();
	}
	@Get("/remove/{id},{descricao}")
	public void remove(Long id, String descricao){
		dao.remove(id, descricao);
		result.include("mensagem","Veiculo modelo: " + descricao + " removido com sucesso! ");
		result.redirectTo(this).veiculo();
	}
	
	@Get("/excluir/{id},{descricao}")
	public void excluir(Long id, String descricao){
		Veiculo veiculoEncontrado = dao.buscaPorId(id);
		result.include("veiculo", veiculoEncontrado);
		
	}
}