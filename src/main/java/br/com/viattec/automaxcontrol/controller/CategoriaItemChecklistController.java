package br.com.viattec.automaxcontrol.controller;

import java.util.List;

import javax.inject.Inject;
import javax.validation.Valid;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.validator.Validator;
import br.com.viattec.automaxcontrol.dao.CategoriaItemChecklistDao;
import br.com.viattec.automaxcontrol.model.CategoriaItemChecklist;
import br.com.viattec.automaxcontrol.model.ItemCheklist;


@Controller
@Path("/categoriaitemchecklist")
public class CategoriaItemChecklistController {
	
	private final Result result;
	private final CategoriaItemChecklistDao dao;
	private final Validator validator;

	@Inject
	public CategoriaItemChecklistController(Result result, CategoriaItemChecklistDao dao, Validator validator) {
		this.result = result;
		this.dao = dao;
		this.validator = validator;
	}	
	
	
	public CategoriaItemChecklistController() {
		this(null,null,null);
	}
	@Get("/categoriaitemchecklist")
	public void categoriaItemChecklist() {
		List<CategoriaItemChecklist> lista = dao.lista();
		result.include("categoriaItemChecklistList",lista);
	}
	@Post 
	public void adiciona(@Valid CategoriaItemChecklist categoriaItemChecklist) {
		validator.onErrorForwardTo(this).categoriaItemChecklist();
		dao.add(categoriaItemChecklist);
		result.include("mensagem","Categoria do Item do checklist: " + categoriaItemChecklist.getDescricao() + " cadastrada com sucesso!");
		result.redirectTo(this).categoriaItemChecklist();
	}
	@Get("/alterar/{id}")
	public void alterar(Long id) {
		CategoriaItemChecklist categoriaItemChecklistEncrontrada = dao.buscaPorId(id);		
		result.include("categoriaItemChecklist", categoriaItemChecklistEncrontrada);
	}
	@Post
	public void atualiza(CategoriaItemChecklist categoriaItemChecklist) {
		dao.update(categoriaItemChecklist);
		result.redirectTo(this).categoriaItemChecklist();
	}
	@Get("/remove/{id},{descricao}")
	public void remove(Long id, String descricao){
		dao.remove(id, descricao);
		result.include("mensagem","Categoria do item do Checklist: " + descricao + " excluída com sucesso! ");
		result.redirectTo(this).categoriaItemChecklist();
	}
	
	@Get("/excluir/{id},{descricao}")
	public void excluir(Long id, String descricao){
		CategoriaItemChecklist categoriaItemChecklistEncrontrada = dao.buscaPorId(id);		
		result.include("categoriaItemChecklist", categoriaItemChecklistEncrontrada);
		
	}

}
