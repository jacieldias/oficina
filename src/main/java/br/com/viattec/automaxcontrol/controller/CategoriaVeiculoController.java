package br.com.viattec.automaxcontrol.controller;

import java.util.List;

import javax.inject.Inject;
import javax.validation.Valid;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.validator.Validator;
import br.com.viattec.automaxcontrol.dao.CategoriaVeiculoDao;
import br.com.viattec.automaxcontrol.model.CategoriaVeiculo;
import br.com.viattec.automaxcontrol.model.Montadora;

@Controller
@Path("/categoriaveiculo")
public class CategoriaVeiculoController {

	private final Result result;
	private final CategoriaVeiculoDao dao;
	private final Validator validator;

	@Inject
	public CategoriaVeiculoController(Result result, CategoriaVeiculoDao dao, Validator validator) {
		this.result = result;
		this.dao = dao;
		this.validator = validator;
	}	
	
	
	public CategoriaVeiculoController() {
		this(null,null,null);
	}
	@Get("/categoria")
	public void categoriaVeiculo() {
		List<CategoriaVeiculo> lista = dao.lista();
		result.include("categoriaVeiculoList",lista);
	}
	@Post 
	public void adiciona(@Valid CategoriaVeiculo categoriaVeiculo) {
		validator.onErrorForwardTo(this).categoriaVeiculo();
		dao.add(categoriaVeiculo);
		result.include("mensagem","Categoria do veículo: " + categoriaVeiculo.getDescricao() + " cadastrada com sucesso!");
		result.redirectTo(this).categoriaVeiculo();
	}
	@Get("/alterar/{id}")
	public void alterar(Long id) {
		CategoriaVeiculo categoriaVeiculoEncrontrada = dao.buscaPorId(id);		
		result.include("categoriaVeiculo",categoriaVeiculoEncrontrada);
	}
	@Post
	public void atualiza(CategoriaVeiculo categoriaVeiculo) {
		dao.update(categoriaVeiculo);
		result.redirectTo(this).categoriaVeiculo();
	}
	@Get("/remove/{id},{descricao}")
	public void remove(Long id, String descricao){
		dao.remove(id, descricao);
		result.include("mensagem","Categoria do veículo: " + descricao + " excluída com sucesso! ");
		result.redirectTo(this).categoriaVeiculo();
	}
	
	@Get("/excluir/{id},{descricao}")
	public void excluir(Long id, String descricao){
		CategoriaVeiculo categoriaVeiculoEncontrado = dao.buscaPorId(id);
		result.include("categoriaVeiculo", categoriaVeiculoEncontrado);
		
	}
	
}

