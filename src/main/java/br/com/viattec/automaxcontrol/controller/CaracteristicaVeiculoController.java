package br.com.viattec.automaxcontrol.controller;

import java.util.List;

import javax.inject.Inject;
import javax.validation.Valid;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.validator.Validator;
import br.com.viattec.automaxcontrol.dao.CaracteristicaVeiculoDao;
import br.com.viattec.automaxcontrol.model.CaracteristicaVeiculo;
import br.com.viattec.automaxcontrol.model.CategoriaVeiculo;

@Controller
@Path("/caracteristicaveiculo")
public class CaracteristicaVeiculoController {

	private final Result result;
	private final CaracteristicaVeiculoDao dao;
	private final Validator validator;

	@Inject
	public CaracteristicaVeiculoController(Result result, CaracteristicaVeiculoDao dao, Validator validator) {
		this.result = result;
		this.dao = dao;
		this.validator = validator;
	}	
	
	
	public CaracteristicaVeiculoController() {
		this(null,null,null);
	}
	@Get("/caracteristica")
	public void caracteristicaVeiculo() {
		List<CaracteristicaVeiculo> lista = dao.lista();
		result.include("caracteristicaVeiculoList",lista);
	}
	@Post 
	public void adiciona(@Valid CaracteristicaVeiculo caracteristicaVeiculo) {
		validator.onErrorForwardTo(this).caracteristicaVeiculo();
		dao.add(caracteristicaVeiculo);
		result.include("mensagem","Caracteristica do veículo: " + caracteristicaVeiculo.getDescricao() + " cadastrada com sucesso!");
		result.redirectTo(this).caracteristicaVeiculo();
	}
	@Get("/alterar/{id}")
	public void alterar(Long id) {
		CaracteristicaVeiculo caracteristicaVeiculoEncrontrada = dao.buscaPorId(id);		
		result.include("caracteristicaVeiculo",caracteristicaVeiculoEncrontrada);
		
	}
	@Post
	public void atualiza(CaracteristicaVeiculo caracteristicaVeiculo) {
		dao.update(caracteristicaVeiculo);
		result.redirectTo(this).caracteristicaVeiculo();
	}
	@Get("/remove/{id},{descricao}")
	public void remove(Long id, String descricao){
		dao.remove(id, descricao);
		result.include("mensagem","Caracteristica do veículo: " + descricao + " removida com sucesso!");
		result.redirectTo(this).caracteristicaVeiculo();
	}
	
	@Get("/excluir/{id},{descricao}")
	public void excluir(Long id, String descricao){
		CaracteristicaVeiculo caracteristicaVeiculoEncontrado = dao.buscaPorId(id);
		result.include("caracteristicaVeiculo", caracteristicaVeiculoEncontrado);
		
	}
}

