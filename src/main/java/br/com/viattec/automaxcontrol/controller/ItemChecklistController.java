package br.com.viattec.automaxcontrol.controller;

import java.util.List;

import javax.inject.Inject;
import javax.validation.Valid;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.validator.Validator;
import br.com.viattec.automaxcontrol.dao.ItemChecklistDao;
import br.com.viattec.automaxcontrol.model.ItemCheklist;
import br.com.viattec.automaxcontrol.model.Montadora;


@Controller
@Path("/itemchecklist")
public class ItemChecklistController {
	
	private final Result result;
	private final ItemChecklistDao dao;
	private final Validator validator;

	@Inject
	public ItemChecklistController(Result result, ItemChecklistDao dao, Validator validator) {
		this.result = result;
		this.dao = dao;
		this.validator = validator;
	}	
	
	
	public ItemChecklistController() {
		this(null,null,null);
	}
	@Get("/itemchecklist")
	public void itemChecklist() {
		List<ItemCheklist> lista = dao.lista();
		result.include("itemChecklistList",lista);
	}
	@Post 
	public void adiciona(@Valid ItemCheklist itemChecklist) {
		validator.onErrorForwardTo(this).itemChecklist();
		dao.add(itemChecklist);
		result.include("mensagem","Item do checklist: " + itemChecklist.getDescricao() + " cadastrado com sucesso!");
		result.redirectTo(this).itemChecklist();
	}
	@Get("/alterar/{id}")
	public void alterar(Long id) {
		ItemCheklist itemChecklistEncrontrada = dao.buscaPorId(id);		
		result.include("itemChecklist", itemChecklistEncrontrada);
	}
	@Post
	public void atualiza(ItemCheklist itemChecklist) {
		dao.update(itemChecklist);
		result.redirectTo(this).itemChecklist();
	}
	@Get("/remove/{id},{descricao}")
	public void remove(Long id, String descricao){
		dao.remove(id, descricao);
		result.include("mensagem","Item do Checklist: " + descricao + " excluído com sucesso! ");
		result.redirectTo(this).itemChecklist();
	}
	
	@Get("/excluir/{id},{descricao}")
	public void excluir(Long id, String descricao){
		ItemCheklist itemChecklistEncrontrada = dao.buscaPorId(id);		
		result.include("itemChecklist", itemChecklistEncrontrada);
		
	}

}
