package br.com.viattec.automaxcontrol.controller;

import java.util.List;

import javax.inject.Inject;
import javax.validation.Valid;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.validator.Validator;
import br.com.viattec.automaxcontrol.dao.MontadoraDao;
import br.com.viattec.automaxcontrol.model.Montadora;

@Controller
@Path("/montadora")
public class MontadoraController {

	private final Result result;
	private final MontadoraDao dao;
	private final Validator validator;

	@Inject
	public MontadoraController(Result result,MontadoraDao dao, Validator validator) {
		this.result = result;
		this.dao = dao;
		this.validator = validator;
	}	
	public MontadoraController() {
		this(null,null,null);
	}
	@Get("/montadora")
	public void montadora() {
		List<Montadora> lista = dao.lista();
		result.include("montadoraList",lista);
	}
	@Post
	public void adiciona(@Valid Montadora montadora) {
		validator.onErrorForwardTo(this).montadora();
		dao.add(montadora);
		result.include("mensagem","Montadora: " + montadora.getDescricao() + " cadastrada com sucesso!");
		result.redirectTo(this).montadora();
	}
	@Get("/alterar/{id}")
	public void alterar(Long id) {
		Montadora montadoraEncrontrada = dao.buscaPorId(id);		
		result.include("montadora",montadoraEncrontrada);
		
	}
	@Post
	public void atualiza(Montadora montadora) {
		dao.update(montadora);
		result.redirectTo(this).montadora();		
	}
	@Get("/remove/{id},{descricao}")
	public void remove(Long id, String descricao){
		dao.remove(id, descricao);
		result.include("mensagem","Montadora: " + descricao + " excluída com sucesso! ");
		result.redirectTo(this).montadora();
		
	}
	
	@Get("/excluir/{id},{descricao}")
	public void excluir(Long id, String descricao){
		Montadora montadoraEncrontrada = dao.buscaPorId(id);
		result.include("montadora", montadoraEncrontrada);
		
	}
}