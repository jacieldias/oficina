package br.com.viattec.automaxcontrol.controller;

import java.util.List;

import javax.inject.Inject;
import javax.validation.Valid;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.validator.Validator;
import br.com.viattec.automaxcontrol.dao.ClienteDao;
import br.com.viattec.automaxcontrol.model.Cliente;
import br.com.viattec.automaxcontrol.model.Estados;
import br.com.viattec.automaxcontrol.model.Montadora;
import br.com.viattec.automaxcontrol.model.SituacaoCliente;

@Controller
@Path("/cliente")
public class ClienteController {
	
	private final Result result;
	private final ClienteDao dao;
	private final Validator validator;

	@Inject
	public ClienteController(Result result, ClienteDao dao, Validator validator) {
		this.result = result;
		this.dao = dao;
		this.validator = validator;
	}	
	public ClienteController() {
		this(null,null,null);
	}
	@Get("/cliente")
	public void cliente() {
		List<Cliente> lista = dao.lista();
		result.include("clienteList", lista);
		result.include("enumEstados", Estados.values());
		result.include("enumSituacao", SituacaoCliente.values());
	}
	@Post
	public void adiciona(@Valid Cliente cliente) {
		validator.onErrorForwardTo(this).cliente();
		dao.add(cliente);
		result.include("mensagem","Cliente: " + cliente.getNome() + " cadastrado com sucesso!");
		result.redirectTo(this).cliente();
	}
	@Get("/alterar/{id}")
	public void alterar(Long id) {
		Cliente clienteEncrontrada = dao.buscaPorId(id);		
		result.include("cliente",clienteEncrontrada);
		result.include("enumEstados", Estados.values());
		result.include("enumSituacao", SituacaoCliente.values());
	}
	@Post
	public void atualiza(Cliente cliente) {
		dao.update(cliente);
		result.redirectTo(this).cliente();
	}
	@Get("/remove/{id},{nome}")
	public void remove(Long id, String nome){
		dao.remove(id, nome);
		result.include("mensagem","Cliente: " + nome + " excluído com sucesso! ");
		result.redirectTo(this).cliente();
	}
	
	@Get("/consultar/{id}")
	public void consultar(Long id) {
		Cliente clienteEncrontrada = dao.buscaPorId(id);		
		result.include("cliente",clienteEncrontrada);
		result.include("enumEstados", Estados.values());
		result.include("enumSituacao", SituacaoCliente.values());
	}
	
	@Get("/excluir/{id},{nome}")
	public void excluir(Long id, String nome){
		Cliente clienteEncontrado = dao.buscaPorId(id);
		result.include("cliente", clienteEncontrado);
		
	}

}
