package br.com.viattec.automaxcontrol.dao;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.viattec.automaxcontrol.model.CategoriaVeiculo;

public class CategoriaVeiculoDao {
	
	private final EntityManager em;
	
	@Inject
	public CategoriaVeiculoDao(EntityManager em){
		this.em = em;
	}
	
	public CategoriaVeiculoDao (){
		this(null);
	}
	
	public void add(CategoriaVeiculo categoriaVeiculo){
		em.getTransaction().begin();
		em.persist(categoriaVeiculo);
		em.getTransaction().commit();
	}
	
	public void update(CategoriaVeiculo categoriaVeiculo){
		em.getTransaction().begin();
		CategoriaVeiculo categoriaVeiculoBD = em.find(CategoriaVeiculo.class, categoriaVeiculo.getId());
		categoriaVeiculoBD.setDescricao(categoriaVeiculo.getDescricao());
		em.merge(categoriaVeiculoBD);
		em.getTransaction().commit();
	}
	
	public void remove(Long id, String descricao){
		em.getTransaction().begin();
		CategoriaVeiculo categoriaVeiculoBD = em.find(CategoriaVeiculo.class, id);
		em.remove(categoriaVeiculoBD);
		em.getTransaction().commit();
	}
	
	public CategoriaVeiculo buscaPorId(Long id) {
		return em.find(CategoriaVeiculo.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public List<CategoriaVeiculo> lista() {
		return em.createQuery("select c from CategoriaVeiculo c").getResultList();
	}	
}
