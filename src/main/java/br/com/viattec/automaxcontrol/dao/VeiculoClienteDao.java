package br.com.viattec.automaxcontrol.dao;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import br.com.viattec.automaxcontrol.model.VeiculoCliente;

public class VeiculoClienteDao {
	
	private EntityManager em;
	
	@Inject
	public VeiculoClienteDao(EntityManager em){
		this.em = em;
	}
	
	public VeiculoClienteDao(){
		this(null);
	}
	
	public void add(VeiculoCliente veiculoCliente){
		em.getTransaction().begin();
		em.persist(veiculoCliente);
		em.getTransaction().commit();
	}
	
	public void update(VeiculoCliente veiculoCliente){
		em.getTransaction().begin();
		VeiculoCliente veiculoClienteBD = em.find(VeiculoCliente.class, veiculoCliente.getId());
		veiculoClienteBD.setCliente(veiculoCliente.getCliente());
		veiculoClienteBD.setVeiculo(veiculoCliente.getVeiculo());
		veiculoClienteBD.setPlaca(veiculoCliente.getPlaca());
		veiculoClienteBD.setNumeroChassi(veiculoCliente.getNumeroChassi());
		veiculoClienteBD.setCorPredominante(veiculoCliente.getCorPredominante());
		veiculoClienteBD.setAnoFabricacao(veiculoCliente.getAnoFabricacao());
		veiculoClienteBD.setCaracteristicas(veiculoCliente.getCaracteristicas());
		em.getTransaction().commit();
	}
	
	public void remove(Long id){
		em.getTransaction().begin();
		VeiculoCliente veiculoClienteBD = em.find(VeiculoCliente.class,id);
		em.remove(veiculoClienteBD);
		em.getTransaction().commit();
	}
	
	public VeiculoCliente buscaPorId(Long id){
		return em.find(VeiculoCliente.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public List<VeiculoCliente> lista() {
		return em.createQuery("select v from VeiculoCliente v").getResultList();
	}
}
