package br.com.viattec.automaxcontrol.dao;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.viattec.automaxcontrol.model.ItemCheklist;

@PersistenceContext
public class ItemChecklistDao {
	
	private final EntityManager em;
	
	@Inject
	public ItemChecklistDao(EntityManager em){
		this.em = em;
	}
	
	public ItemChecklistDao(){
		this(null);
	}
	
	public void add(ItemCheklist itemCheklist){
		em.getTransaction().begin();
		em.persist(itemCheklist);
		em.getTransaction().commit();
	}
	
	public void update(ItemCheklist itemCheklist){
		em.getTransaction().begin();
		ItemCheklist itemCheklistBD = em.find(ItemCheklist.class,itemCheklist.getId());
		itemCheklistBD.setDescricao(itemCheklist.getDescricao());
		em.merge(itemCheklistBD);
		em.getTransaction().commit();
	}
	
	public void remove(Long id, String descricao){
		em.getTransaction().begin();
		ItemCheklist itemCheklistBD = em.find(ItemCheklist.class,id);
		em.remove(itemCheklistBD);
		em.getTransaction().commit();
	}
	
	public ItemCheklist buscaPorId(Long id){
		return em.find(ItemCheklist.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public List<ItemCheklist> lista(){
		return em.createQuery("select i from ItemCheklist i").getResultList();
	}
}
