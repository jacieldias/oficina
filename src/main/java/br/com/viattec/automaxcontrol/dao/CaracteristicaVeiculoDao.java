package br.com.viattec.automaxcontrol.dao;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.viattec.automaxcontrol.model.CaracteristicaVeiculo;

public class CaracteristicaVeiculoDao {
	
	private final EntityManager em;
	
	@Inject
	public CaracteristicaVeiculoDao(EntityManager em){
		this.em = em;
	}
	
	public CaracteristicaVeiculoDao(){
		this(null);
	}
	
	public void add(CaracteristicaVeiculo caracteristicaVeiculo){
		em.getTransaction().begin();
		em.persist(caracteristicaVeiculo);
		em.getTransaction().commit();
	}
	
	public void update(CaracteristicaVeiculo caracteristicaVeiculo){
		em.getTransaction().begin();
		CaracteristicaVeiculo caracteristicaVeiculoBD = em.find(CaracteristicaVeiculo.class, caracteristicaVeiculo.getId());
		caracteristicaVeiculoBD.setDescricao(caracteristicaVeiculo.getDescricao());
		em.merge(caracteristicaVeiculoBD);
		em.getTransaction().commit();
	}
	
	public void remove(Long id, String descricao){
		em.getTransaction().begin();
		CaracteristicaVeiculo caracteristicaVeiculoBD = em.find(CaracteristicaVeiculo.class, id);
		em.remove(caracteristicaVeiculoBD);
		em.getTransaction().commit();
	}
	
	public CaracteristicaVeiculo buscaPorId(Long id){
		return em.find(CaracteristicaVeiculo.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public List<CaracteristicaVeiculo> lista(){
		return em.createQuery("select c from CaracteristicaVeiculo c").getResultList();
	}
}
