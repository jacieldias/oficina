package br.com.viattec.automaxcontrol.dao;

import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.viattec.automaxcontrol.model.Montadora;


@PersistenceContext
public class MontadoraDao {

	private final EntityManager em;

	@Inject
	public MontadoraDao(EntityManager em) {
		this.em = em;
	}

	public MontadoraDao() {
		this(null);
	}

	public void add(Montadora montadora) {
		em.getTransaction().begin();
		em.persist(montadora);
		em.getTransaction().commit();
	}

	public void update(Montadora montadora) {
		Montadora montadoraBD = new Montadora();
		em.getTransaction().begin();
		montadoraBD = em.find(Montadora.class, montadora.getId());
		montadoraBD.setDescricao(montadora.getDescricao());
		em.merge(montadoraBD);
		em.getTransaction().commit();
	}
	
	public void remove(Long id, String descricao) {
		em.getTransaction().begin();
		Montadora montadoraBD = em.find(Montadora.class, id);
		em.remove(montadoraBD);
		em.getTransaction().commit();
	}

	public Montadora buscaPorId(Long id) {
		return em.find(Montadora.class, id);
	}

	@SuppressWarnings("unchecked")
	public List<Montadora> lista() {
		return em.createQuery("select m from Montadora m").getResultList();
	}
}
