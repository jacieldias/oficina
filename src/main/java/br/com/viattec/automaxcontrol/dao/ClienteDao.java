package br.com.viattec.automaxcontrol.dao;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.viattec.automaxcontrol.model.Cliente;

public class ClienteDao {
	
	private final EntityManager em;
	
	@Inject
	public ClienteDao(EntityManager em){
		this.em = em;
	}
	
	public ClienteDao(){
		this(null);
	}
	
	public void add(Cliente cliente){
		em.getTransaction().begin();
		em.persist(cliente);
		em.getTransaction().commit();
	}
	
	public void update(Cliente cliente){
		em.getTransaction().begin();
		Cliente clienteBD = em.find(Cliente.class, cliente.getId());
		clienteBD.setNome(cliente.getNome());
		clienteBD.setLogradouro(cliente.getLogradouro());
		clienteBD.setNumero(cliente.getNumero());
		clienteBD.setBairro(cliente.getBairro());
		clienteBD.setCidade(cliente.getCidade());
		clienteBD.setUf(cliente.getUf());
		clienteBD.setTelefone1(cliente.getTelefone1());
		clienteBD.setTelefone2(cliente.getTelefone2());
		clienteBD.setTelefone3(cliente.getTelefone3());
		clienteBD.setSituacao(cliente.getSituacao());
		clienteBD.setDataInclusao(cliente.getDataInclusao());
		clienteBD.setCpf(cliente.getCpf());
		clienteBD.setRg(cliente.getRg());
		clienteBD.setCep(cliente.getCep());
		em.merge(clienteBD);
		em.getTransaction().commit();
	}
	
	public void remove(Long id, String nome){
		em.getTransaction().begin();
		Cliente clienteBD = em.find(Cliente.class, id);
		em.remove(clienteBD);
		em.getTransaction().commit();
	}
	
	public Cliente buscaPorId(Long id){
		return em.find(Cliente.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public List<Cliente> lista(){
		return em.createQuery("select c from Cliente c").getResultList();
	}

}
