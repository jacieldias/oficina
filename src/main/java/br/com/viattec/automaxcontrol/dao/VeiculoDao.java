package br.com.viattec.automaxcontrol.dao;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.viattec.automaxcontrol.model.Veiculo;

public class VeiculoDao {

	private EntityManager em;

	@Inject
	public VeiculoDao(EntityManager em) {
		this.em = em;
	}
	
	public VeiculoDao(){
		this(null);
	}
	
	public void add(Veiculo veiculo){
		em.getTransaction().begin();
		em.persist(veiculo);
		em.getTransaction().commit();
	}
	
	public void update(Veiculo veiculo){
		em.getTransaction().begin();
		Veiculo veiculoBD = em.find(Veiculo.class, veiculo.getId());
		veiculoBD.setDescricao(veiculo.getDescricao());
		veiculoBD.setMontadora(veiculo.getMontadora());
		veiculoBD.setCategoriaVeiculo(veiculo.getCategoriaVeiculo());
		em.merge(veiculoBD);
		em.getTransaction().commit();
	}
	
	public void remove(Long id, String descricao){
		em.getTransaction().begin();
		Veiculo veiculoBD = em.find(Veiculo.class, id);
		em.remove(veiculoBD);
		em.getTransaction().commit();
	}
	
	public Veiculo buscaPorId(Long id){
		return em.find(Veiculo.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public List<Veiculo> lista() {
		return em.createQuery("select v from Veiculo v").getResultList();
	}
}
