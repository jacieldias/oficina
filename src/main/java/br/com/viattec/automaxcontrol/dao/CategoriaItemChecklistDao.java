package br.com.viattec.automaxcontrol.dao;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.viattec.automaxcontrol.model.CategoriaItemChecklist;

@PersistenceContext
public class CategoriaItemChecklistDao {
	
	private final EntityManager em;
	
	@Inject
	public CategoriaItemChecklistDao(EntityManager em){
		this.em = em;
	}
		
	public CategoriaItemChecklistDao(){
		this(null);
	}
	
	public void add(CategoriaItemChecklist categoriaItemChecklist){
		em.getTransaction().begin();
		em.persist(categoriaItemChecklist);
		em.getTransaction().commit();
	}
	
	public void update(CategoriaItemChecklist categoriaItemChecklist){
		em.getTransaction().begin();
		CategoriaItemChecklist categoriaItemChecklistBD = em.find(CategoriaItemChecklist.class,categoriaItemChecklist.getId());
		categoriaItemChecklistBD.setDescricao(categoriaItemChecklist.getDescricao());
		em.merge(categoriaItemChecklistBD);
		em.getTransaction().commit();
	}
	
	public void remove(Long id, String descricao){
		em.getTransaction().begin();
		CategoriaItemChecklist categoriaItemChecklistBD = em.find(CategoriaItemChecklist.class,id);
		em.remove(categoriaItemChecklistBD);
		em.getTransaction().commit();
	}
	
	public CategoriaItemChecklist buscaPorId(Long id){
		return em.find(CategoriaItemChecklist.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public List<CategoriaItemChecklist> lista(){
		return em.createQuery("select c from CategoriaItemChecklist c").getResultList();
	}
}
