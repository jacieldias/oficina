package br.com.viattec.automaxcontrol.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class ItemCheklist {

	@Id
	@SequenceGenerator(name="item_checklist_id", sequenceName="item_checklist_seq", allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="item_checklist_id")
	private Long id;
	@NotNull(message="{descricao.vazia}") 
	@NotEmpty(message="{descricao.vazia}")
	private String descricao;
	@NotNull
	@ManyToOne
	private CategoriaItemChecklist categoriaItemChecklist;

	public ItemCheklist(){
		
	}
	
	public ItemCheklist(Long id, String descricao,
			CategoriaItemChecklist categoriaItemChecklist) {
		super();
		this.id = id;
		this.descricao = descricao;
		this.categoriaItemChecklist = categoriaItemChecklist;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public CategoriaItemChecklist getCategoriaItemChecklist() {
		return categoriaItemChecklist;
	}

	public void setCategoriaItemChecklist(
			CategoriaItemChecklist categoriaItemChecklist) {
		this.categoriaItemChecklist = categoriaItemChecklist;
	}
}
