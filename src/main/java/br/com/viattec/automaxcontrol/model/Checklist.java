package br.com.viattec.automaxcontrol.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

@Entity
public class Checklist {
	
	@Id
	@SequenceGenerator(name = "checklist_id", sequenceName = "checklist_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="checklist_id")
	private Long id;
	@NotNull
	@ManyToOne
	private Cliente cliente;
	@NotNull
	@ManyToOne
	private Veiculo veiculo;
	@ManyToMany
	private List<ItemCheklist> itens;
	
	public Checklist(){
		
	}
	
	public Checklist(Cliente cliente, Veiculo veiculo, List<ItemCheklist> itens) {
		super();
		this.cliente = cliente;
		this.veiculo = veiculo;
		this.itens = itens;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Veiculo getVeiculo() {
		return veiculo;
	}

	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}

	public List<ItemCheklist> getItens() {
		return itens;
	}

	public void setItens(List<ItemCheklist> itens) {
		this.itens = itens;
	}
}
