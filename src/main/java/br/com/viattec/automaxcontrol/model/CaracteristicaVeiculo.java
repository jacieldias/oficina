package br.com.viattec.automaxcontrol.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class CaracteristicaVeiculo {
	
	@Id
	@SequenceGenerator(name="caracteristica_veiculo_id",sequenceName="caracteristica_veiculo_seq",allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="caracteristica_veiculo_id")
	private Long id;
	@NotNull(message="{descricao.vazia}") 
	@NotEmpty(message="{descricao.vazia}")
	private String descricao;
	
	public CaracteristicaVeiculo(){
		
	}
	
	public CaracteristicaVeiculo(String descricao){
		super();
		this.descricao = descricao;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}
