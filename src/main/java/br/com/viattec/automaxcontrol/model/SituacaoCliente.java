package br.com.viattec.automaxcontrol.model;

public enum SituacaoCliente {
	
	ATIVO("ATIVO"), 
	BLOQUEADO("BLOQUEADO");
	
	private String label;

	private SituacaoCliente(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	
	
}
