package br.com.viattec.automaxcontrol.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class CategoriaItemChecklist {
	
	@Id
	@SequenceGenerator(name="categoria_item_checklist_id", sequenceName="categoria_item_checklist_seq", allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="categoria_item_checklist_id")
	private Long id;
	@NotNull(message="{descricao.vazia}") 
	@NotEmpty(message="{descricao.vazia}")
	private String descricao;
	
	public CategoriaItemChecklist(){
		
	}
	
	public CategoriaItemChecklist(String descricao) {
		super();
		this.descricao = descricao;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}
