package br.com.viattec.automaxcontrol.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;


@Entity
public class Veiculo {
	
	@Id
	@SequenceGenerator(name = "veiculo_id", sequenceName = "veiculo_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="veiculo_id")
	private Long id;
	
	@NotNull(message="{descricao.vazia}") 
	@NotEmpty(message="{descricao.vazia}")
	private String descricao;

	@NotNull
	@ManyToOne
	private Montadora montadora;
	@NotNull
	@ManyToOne
	private CategoriaVeiculo categoriaVeiculo;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Montadora getMontadora() {
		return montadora;
	}
	public void setMontadora(Montadora montadora) {
		this.montadora = montadora;
	}
	public CategoriaVeiculo getCategoriaVeiculo() {
		return categoriaVeiculo;
	}
	public void setCategoriaVeiculo(CategoriaVeiculo categoriaVeiculo) {
		this.categoriaVeiculo = categoriaVeiculo;
	}
}
