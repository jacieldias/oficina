package br.com.viattec.automaxcontrol.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

@SuppressWarnings("serial")
@Entity
public class Montadora implements Serializable{

	@Id
	@SequenceGenerator(name = "montadora_id", sequenceName = "montadora_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="montadora_id")
	private Long id;
	@NotNull(message="{descricao.vazia}") 
	@NotEmpty(message="{descricao.vazia}")
	private String descricao;

	public Montadora(){}
	
	public Montadora(String descricao) {
		super();
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
}
