package br.com.viattec.automaxcontrol.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

@Entity
public class VeiculoCliente {

	@Id
	@SequenceGenerator(name = "veiculo_cliente_id", sequenceName = "veiculo_cliente_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "veiculo_cliente_id")
	private Long id;
	@NotNull
	@ManyToOne
	private Cliente cliente;
	@NotNull
	@OneToOne
	private Veiculo veiculo;
	@NotNull
	private String placa;
	private String numeroChassi;
	private String corPredominante;
	private int anoFabricacao;
	@OneToMany
	@JoinTable(name = "clienteveiculo_caracteristica", joinColumns = { @JoinColumn(name = "veiculo_id", referencedColumnName = "id") }, inverseJoinColumns = { @JoinColumn(name = "caracteristica_id", referencedColumnName = "id") })
	private List<CaracteristicaVeiculo> caracteristicas;

	public VeiculoCliente() {

	}

	public VeiculoCliente(Long id, Cliente cliente, Veiculo veiculo,
			String placa, String numeroChassi, String corPredominante,
			int anoFabricacao) {
		super();
		this.id = id;
		this.cliente = cliente;
		this.veiculo = veiculo;
		this.placa = placa;
		this.numeroChassi = numeroChassi;
		this.corPredominante = corPredominante;
		this.anoFabricacao = anoFabricacao;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Veiculo getVeiculo() {
		return veiculo;
	}

	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getNumeroChassi() {
		return numeroChassi;
	}

	public void setNumeroChassi(String numeroChassi) {
		this.numeroChassi = numeroChassi;
	}

	public String getCorPredominante() {
		return corPredominante;
	}

	public void setCorPredominante(String corPredominante) {
		this.corPredominante = corPredominante;
	}

	public int getAnoFabricacao() {
		return anoFabricacao;
	}

	public void setAnoFabricacao(int anoFabricacao) {
		this.anoFabricacao = anoFabricacao;
	}

	public List<CaracteristicaVeiculo> getCaracteristicas() {
		return caracteristicas;
	}

	public void setCaracteristicas(List<CaracteristicaVeiculo> caracteristicas) {
		this.caracteristicas = caracteristicas;
	}
}
